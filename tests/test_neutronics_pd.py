from neutronics_pd import __version__


def test_version():
    assert __version__ == '0.1.5'
